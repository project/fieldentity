<?php

/**
 * @file
 * FieldEntity implementation of the field storage API.
 */

/**********************************************************************
 * Field CRUD API
 **********************************************************************/

/**
 * Implements hook_field_create_field().
 */
function fieldentity_field_create_field($field) {
  $field_info = field_info_field_types($field['type']);
  if (isset($field_info['fieldentity']) && $field['storage']['type'] != 'fieldentity') {
    field_delete_field($field['field_name']);
    field_purge_batch(1);
    $field['storage'] = array('type' => 'fieldentity');
    field_create_field($field);
  }

  if ($field['storage']['type'] == 'fieldentity') {
    field_attach_create_bundle($field['type'], $field['field_name']);
  }
}

/**
 * Implements hook_field_update_field().
 */
function fieldentity_field_update_field($field, $prior_field, $has_data) {
  if ($field['storage']['type'] == 'fieldentity') {
    if ($field['field_name'] != $prior_field['field_name']) {
      field_attach_rename_bundle($field['type'], $field['field_name']);
    }
  }
}

/**
 * Implements hook_field_delete_field().
 */
function fieldentity_field_delete_field($field) {
  if ($field['storage']['type'] == 'fieldentity') {
    field_attach_delete_bundle($field['type'], $field['field_name']);
  }
}

/**********************************************************************
 * Field Storage API
 **********************************************************************/

/**
 * Implements hook_field_storage_info().
 */
function fieldentity_field_storage_info() {
  return array(
    'fieldentity' => array(
      'label' => t('FieldEntity storage'),
      'description' => t('Delegates field(entity) storage to its entity controller.'),
    ),
  );
}

/**
 * Returns info of entity type belonging to a fieldentity field type.
 *
 * @param $field_type
 *   A (entity)field type name.
 *
 * @return
 *   An array containing entity types info.
 */
function _fieldentity_entity_info($field_type) {
  $field_info = field_info_field_types($field_type);
  if (empty($field_info['fieldentity'])) {
    throw new Exception('Unspecified fieldentity directive for field type ' . $field_type);
  }

  $entity_info = entity_get_info($field_info['fieldentity']);
  if (empty($entity_info['fieldentity']) || $entity_info['fieldentity'] != $field_type) {
    throw new Exception('Invalid or missing fieldentity directive for entity type '. $field_info['fieldentity']);
  }

  return $entity_info;
}

/**
 * Returns the table name of a fieldentity data table.
 *
 * @param $field_type
 *   A (entity)field type name.
 *
 * @return
 *   A string containing the generated name for the database table
 */
function _fieldentity_tablename($field_type) {
  static $tablenames = array();
  if (isset($tablenames[$field_type])) {
    return $tablenames[$field_type];
  }

  $entity_info = _fieldentity_entity_info($field_type);
  $tablenames[$field_type] = $entity_info['base table'];
  return $tablenames[$field_type];
}

/**
 * Returns the table name of a fieldentity revision archive table.
 *
 * @param $field_type
 *   A (entity)field type name.
 *
 * @return
 *   A string containing the generated name for the database table
 */
function _fieldentity_revision_tablename($field_type) {
  static $tablenames = array();
  if (isset($tablenames[$field_type])) {
    return $tablenames[$field_type];
  }

  $entity_info = _fieldentity_entity_info($field_type);
  $tablenames[$field_type] = $entity_info['revision table'];
  return $tablenames[$field_type];
}

/**
 * Generates a column name for a fieldentity data table.
 *
 * @param $field_name
 *   The name of the field
 * @param $column
 *   The name of the column
 *
 * @return
 *   A string containing a generated column name for a field data
 *   table that is unique among all other fields.
 */
function _fieldentity_columnname($field_name, $column) {
  return $field_name . '_' . $column;
}

/**
 * Generates an index name for a fieldentity data table.
 *
 * @param $field_name
 *   The name of the field
 * @param $column
 *   The name of the index
 *
 * @return
 *   A string containing a generated index name for a field data
 *   table that is unique among all other fields.
 */
function _fieldentity_indexname($field_name, $index) {
  return $field_name . '_' . $index;
}

/**
 * Implements hook_field_storage_create_field().
 */
function fieldentity_field_storage_create_field($field) {
  // Add columns/indexes specific to this field.
  fieldentity_field_storage_update_field($field, $field, FALSE);
}

/**
 * Implements hook_field_update_forbid().
 */
function fieldentity_field_update_forbid($field, $prior_field, $has_data) {
  if ($has_data && $field['columns'] != $prior_field['columns']) {
    throw new FieldUpdateForbiddenException('fieldentity cannot change the schema for an existing field with data.');
  }
}

/**
 * Implements hook_field_storage_update_field().
 */
function fieldentity_field_storage_update_field($field, $prior_field, $has_data) {
  $table = _fieldentity_tablename($field['type']);
  $revision_table = _fieldentity_revision_tablename($field['type']);

  $prior_table = _fieldentity_tablename($prior_field['type']);
  $prior_revision_table = _fieldentity_revision_tablename($prior_field['type']);

  if (!$has_data) {
    // There is no data for given field. Re-create all field specific columns.
    foreach ($prior_field['columns'] as $name => $attributes) {
      if (!isset($field['columns'][$name]) || $attributes != $field['columns'][$name]) {
        $real_name = _fieldentity_columnname($field['field_name'], $name);
        db_drop_field($prior_table, $real_name);
        db_drop_field($prior_revision_table, $real_name);
      }
    }

    foreach ($field['columns'] as $name => $attributes) {
      if (!isset($prior_field['columns'][$name]) || $attributes != $prior_field['columns'][$name]) {
        $real_name = _fieldentity_columnname($field['field_name'], $name);
        db_add_field($table, $real_name, $attributes);
        db_add_field($revision_table, $real_name, $attributes);
      }
    }
  }

  // Drop all the prior field-specific indexes and create all the new ones,
  // except for all the priors that exist unchanged.
  foreach ($prior_field['indexes'] as $name => $columns) {
    if (!isset($field['indexes'][$name]) || $columns != $field['indexes'][$name]) {
      $real_name = _fieldentity_indexname($field['field_name'], $name);
      db_drop_index($prior_table, $real_name);
      db_drop_index($prior_revision_table, $real_name);
    }
  }

  foreach ($field['indexes'] as $name => $columns) {
    if (!isset($prior_field['indexes'][$name]) || $columns != $prior_field['indexes'][$name]) {
      $real_name = _fieldentity_indexname($field['field_name'], $name);
      $real_columns = array();
      foreach ($columns as $column_name) {
        $real_columns[] = _fieldentity_columnname($field['field_name'], $column_name);
      }

      db_add_index($table, $real_name, $real_columns);
      db_add_index($revision_table, $real_name, $real_columns);
    }
  }

  drupal_get_schema(NULL, TRUE);
}

/**
 * Implements hook_field_storage_delete_field().
 */
function fieldentity_field_storage_delete_field($field) {
  // Mark all data associated with the field for deletion.
  $field['deleted'] = 0;
  $table = _fieldentity_tablename($field['type']);
  $revision_table = _fieldentity_revision_tablename($field['type']);
  db_update($table)
    ->fields(array('deleted' => 1))
    ->condition('field_id', $field['id'])
    ->execute();
  db_update($revision_table)
    ->fields(array('deleted' => 1))
    ->condition('field_id', $field['id'])
    ->execute();
}

/**
 * Implements hook_field_storage_load().
 */
function fieldentity_field_storage_load($entity_type, $entities, $age, $fields, $options) {
  $field_info = field_info_field_by_ids();
  foreach ($fields as $field_id => $ids) {
    $field = $field_info[$field_id];
    $controller = entity_get_controller($field['fieldentity']);

    if ($controller instanceof FieldEntityController) {
      $controller->loadAsFieldOf($entity_type, $entities, $age, $field_id, $options, $ids);
    }
    else {
      throw new Exception('TODO.');
    }
  }
}

/**
 * Implements hook_field_storage_write().
 */
function fieldentity_field_storage_write($entity_type, $entity, $op, $fields) {
  $field_info = field_info_field_by_ids();
  foreach ($fields as $field_id => $ids) {
    $field = $field_info[$field_id];

    entity_get_controller($field['fieldentity'])->saveAsFieldOf($entity_type, $entity, $op, $field_id);
  }
}

/**
 * Implements hook_field_storage_delete().
 *
 * This function deletes data for all fields for an entity from the database.
 */
function fieldentity_field_storage_delete($entity_type, $entity, $fields) {
  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
  $field_info = field_info_field_by_ids();

  foreach (field_info_instances($entity_type, $bundle) as $instance) {
    if (isset($fields[$instance['field_id']])) {
      $field = $field_info[$instance['field_id']];

      fieldentity_field_storage_purge($entity_type, $entity, $field, $instance);
    }
  }
}

/**
 * Implements hook_field_storage_purge().
 *
 * This function deletes data from the database for a single field on
 * an entity.
 */
function fieldentity_field_storage_purge($entity_type, $entity, $field, $instance) {
  entity_get_controller($field['fieldentity'])->deleteAsFieldOf($entity_type, $entity, $field['id']);
}

/**
 * Implements hook_field_storage_query().
 *
 * TODO move logic to controller if possible.
 */
function fieldentity_field_storage_query(EntityFieldQuery $query) {
  $groups = array();
  if ($query->age == FIELD_LOAD_CURRENT) {
    $tablename_function = '_fieldentity_tablename';
    $id_key = 'parent_id';
  }
  else {
    $tablename_function = '_fieldentity_revision_tablename';
    $id_key = 'parent_vid';
  }
  $table_aliases = array();
  // Add tables for the fields used.
  foreach ($query->fields as $key => $field) {
    $entity_info = _fieldentity_entity_info($field['type']);
    $tablename = $tablename_function($field['type']);
    // Every field needs a new table.
    $table_alias = $tablename . $key;
    $table_aliases[$key] = $table_alias;
    if ($key) {
      $select_query->join($tablename, $table_alias, "$table_alias.parent_type = $field_base_table.parent_type AND $table_alias.$id_key = $field_base_table.$id_key");
    }
    else {
      $select_query = db_select($tablename, $table_alias);
      $select_query->addTag('entity_field_access');
      $select_query->addMetaData('base_table', $tablename);
      $select_query->fields($table_alias, array($entity_info['entity keys']['id'], 'parent_id', 'parent_vid', 'parent_bundle', 'field_id'));
      $field_base_table = $table_alias;
    }
    if ($field['cardinality'] != 1) {
      $select_query->distinct();
    }

    $select_query->condition("$table_alias.field_id", (int) $field['id']);
  }

  // Add field conditions.
  foreach ($query->fieldConditions as $key => $condition) {
    $table_alias = $table_aliases[$key];
    $field = $condition['field'];
    // Add the specified condition.
    $sql_field = "$table_alias." . _fieldentity_columnname($field['field_name'], $condition['column']);
    $query->addCondition($select_query, $sql_field, $condition);
    // Add delta / language group conditions.
    foreach (array('delta', 'language') as $column) {
      if (isset($condition[$column . '_group'])) {
        $group_name = $condition[$column . '_group'];
        if (!isset($groups[$column][$group_name])) {
          $groups[$column][$group_name] = $table_alias;
        }
        else {
          $select_query->where("$table_alias.$column = " . $groups[$column][$group_name] . ".$column");
        }
      }
    }
  }

  if (isset($query->deleted)) {
    $select_query->condition("$field_base_table.deleted", (int) $query->deleted);
  }

  // Is there a need to sort the query by property?
  $has_property_order = FALSE;
  foreach ($query->order as $order) {
    if ($order['type'] == 'property') {
      $has_property_order = TRUE;
    }
  }

  if ($query->propertyConditions || $has_property_order) {
    if (empty($query->entityConditions['entity_type']['value'])) {
      throw new EntityFieldQueryException('Property conditions and orders must have an entity type defined.');
    }
    $entity_type = $query->entityConditions['entity_type']['value'];
    $entity_base_table = _field_sql_storage_query_join_entity($select_query, $entity_type, $field_base_table);
    $query->entityConditions['entity_type']['operator'] = '=';
    $query->processProperty($select_query, $entity_base_table);
  }
  $translate = array(
    'bundle' => 'parent_bundle'
  );
  foreach ($query->entityConditions as $key => $condition) {
    $key = isset($translate[$key]) ? $translate[$key] : $key;
    $sql_field = $key == 'entity_type' ? 'fcet.type' : "$field_base_table.$key";
    $query->addCondition($select_query, $sql_field, $condition);
  }

  // Order the query.
  foreach ($query->order as $order) {
    if ($order['type'] == 'entity') {
      $key = $order['specifier'];
      $sql_field = $key == 'entity_type' ? 'fcet.type' : "$field_base_table.$key";
      $select_query->orderBy($sql_field, $order['direction']);
    }
    elseif ($order['type'] == 'field') {
      $specifier = $order['specifier'];
      $field = $specifier['field'];
      $table_alias = $table_aliases[$specifier['index']];
      $sql_field = "$table_alias." . _fieldentity_columnname($field['field_name'], $specifier['column']);
      $select_query->orderBy($sql_field, $order['direction']);
    }
    elseif ($order['type'] == 'property') {
      $select_query->orderBy("$entity_base_table." . $order['specifier'], $order['direction']);
    }
  }

  return $query->finishQuery($select_query, $id_key);
}

/**
 * Implements hook_field_storage_delete_revision().
 *
 * This function actually deletes the data from the database.
 */
function fieldentity_field_storage_delete_revision($entity_type, $entity, $fields) {
  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);

  if (isset($vid)) {
    foreach ($fields as $field_id) {
      $field = field_info_field_by_id($field_id);
      entity_get_controller($field['fieldentity'])->deleteRevisionAsFieldOf($entity_type, $entity, $field['id']);
    }
  }
}

/**
 * Implements hook_field_storage_delete_instance().
 *
 * This function simply marks for deletion all data associated with the field.
 */
function fieldentity_field_storage_delete_instance($instance) {
  $field = field_info_field($instance['field_name']);
  $table_name = _fieldentity_tablename($field['type']);
  $revision_name = _fieldentity_revision_tablename($field['type']);
  db_update($table_name)
    ->fields(array('deleted' => 1))
    ->condition('parent_type', $instance['entity_type'])
    ->condition('parent_bundle', $instance['bundle'])
    ->condition('field_id', $field['id'])
    ->execute();
  db_update($revision_name)
    ->fields(array('deleted' => 1))
    ->condition('parent_type', $instance['entity_type'])
    ->condition('parent_bundle', $instance['bundle'])
    ->condition('field_id', $field['id'])
    ->execute();
}

/**
 * Implements hook_field_attach_rename_bundle().
 */
function fieldentity_field_attach_rename_bundle($entity_type, $bundle_old, $bundle_new) {
  // We need to account for deleted or inactive fields and instances.
  $instances = field_read_instances(array('entity_type' => $entity_type, 'bundle' => $bundle_new), array('include_deleted' => TRUE, 'include_inactive' => TRUE));
  foreach ($instances as $instance) {
    $field = field_info_field_by_id($instance['field_id']);
    if ($field['storage']['type'] == 'fieldentity') {
      $table_name = _fieldentity_tablename($field['type']);
      $revision_name = _fieldentity_revision_tablename($field['type']);
      db_update($table_name)
        ->fields(array('parent_bundle' => $bundle_new))
        ->condition('parent_type', $entity_type)
        ->condition('parent_bundle', $bundle_old)
        ->condition('field_id', $field['id'])
        ->execute();
      db_update($revision_name)
        ->fields(array('parent_bundle' => $bundle_new))
        ->condition('parent_type', $entity_type)
        ->condition('parent_bundle', $bundle_old)
        ->condition('field_id', $field['id'])
        ->execute();
    }
  }
}

/**
 * Implements hook_field_storage_purge_field().
 *
 * All field data items and instances have already been purged, so all that is
 * left are the columns and indexes of this specific field.
 */
function fieldentity_field_storage_purge_field($field) {
  $table_name = _fieldentity_tablename($field['type']);
  $revision_name = _fieldentity_revision_tablename($field['type']);

  foreach ($field['columns'] as $name => $attributes) {
    $real_name = _fieldentity_columnname($field['field_name'], $name);
    db_drop_field($table_name, $real_name);
    db_drop_field($revision_name, $real_name);
  }

  foreach ($field['indexes'] as $name => $columns) {
    $real_name = _fieldentity_indexname($field['field_name'], $name);
    db_drop_index($prior_table, $real_name);
    db_drop_index($prior_revision_table, $real_name);
  }
}

/**
 * Implements hook_field_storage_details().
 */
function fieldentity_field_storage_details($field) {
  $details = array();
  if (!empty($field['columns'])) {
     // Add field columns.
    foreach ($field['columns'] as $column_name => $attributes) {
      $real_name = _fieldentity_columnname($field['field_name'], $column_name);
      $columns[$column_name] = $real_name;
    }
    return array(
      'sql' => array(
        FIELD_LOAD_CURRENT => array(
          _fieldentity_tablename($field['type']) => $columns,
        ),
        FIELD_LOAD_REVISION => array(
          _fieldentity_revision_tablename($field['type']) => $columns,
        ),
      ),
    );
  }
}
