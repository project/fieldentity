<?php

/**
 * FieldEntity controller.
 */
class FieldEntityController extends EntityAPIController implements EntityAPIControllerInterface {


  /**
   * Overridden.
   * @see EntityAPIController#load($ids, $conditions)
   */
  public function load($ids = array(), $conditions = array()) {
    $entities = parent::load($ids, $conditions);
    return $entities;
  }

  /**
   * Loads fieldentity data from base table.
   */
  protected function dbLoad($ids = array(), $conditions = array()) {
    $query = $this->buildQuery($ids, $conditions);

    return $query->execute()->fetchAllAssoc($this->idKey);
  }

  /**
   * Overridden.
   *
   * @param $transaction
   *   Optionally a DatabaseTransaction object to use. Allows overrides to pass
   *   in their transaction object.
   *
   * @see EntityAPIController#delete($ids)
   */
  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    $entities = $this->load($ids);
    $transaction = isset($transaction) ? $transaction : db_transaction();

    try {

      $this->dbDelete($ids);

      foreach ($entities as $entity) {
        $this->invoke('delete', $entity);

        $this->dbUpdateDeltas($entity->parent_type, $entity->parent_id, $entity->parent_vid, $entity->delta);
      }

      // Ignore slave server temporarily.
      db_ignore_slave();
    }
    catch (Exception $e) {
      $transaction->rollback($this->entityType, $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    $this->resetCache();
  }

  protected function dbDelete($ids) {
    db_delete($this->entityInfo['base table'])
      ->condition($this->nameKey, $ids, 'IN')
      ->execute();
    db_delete($this->entityInfo['revision table'])
      ->condition($this->nameKey, $ids, 'IN')
      ->execute();
  }

  protected function dbUpdateDeltas($parent_type, $parent_id, $parent_vid, $removed_delta) {
    db_update($this->entityInfo['base table'])
      ->fields(array('delta' => 'delta - 1'))
      ->condition('parent_type', $parent_type)
      ->condition('parent_id', $parent_id)
      ->condition('delta', $removed_delta, '>')
      ->execute();
    db_update($this->entityInfo['revision table'])
      ->fields(array('delta' => 'delta - 1'))
      ->condition('parent_type', $parent_type)
      ->condition('parent_id', $parent_id)
      ->condition('parent_vid', $parent_vid)
      ->condition('delta', $removed_delta, '>')
      ->execute();
  }

  /**
   * Overridden.
   *
   * TODO ensure correct deltas of fieldentities of same field.
   *
   * @param $transaction
   *   Optionally a DatabaseTransaction object to use. Allows overrides to pass
   *   in their transaction object.
   *
   * @see EntityAPIController#save($entity)
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    $transaction = isset($transaction) ? $transaction : db_transaction();

    try {
      $this->invoke('presave', $entity);

      $operation = (!empty($entity->{$this->idKey}) && empty($entity->is_new)) ? 'update' : 'insert';
      $return = $this->dbSave($entity);

      $this->invoke($operation, $entity);

      // Ignore slave server temporarily.
      db_ignore_slave();

      return $return;
    }
    catch (Exception $e) {
      $transaction->rollback($this->entityType, $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
  }

  protected function dbSave($entity) {
    if (!empty($entity->{$this->idKey}) && empty($entity->is_new)) {
      $return = drupal_write_record($this->entityInfo['base table'], $entity, $this->idKey);
      drupal_write_record($this->entityInfo['revision table'], $entity, $this->revisionKey);
    }
    else {
      $return = drupal_write_record($this->entityInfo['base table'], $entity);
      drupal_write_record($this->entityInfo['revision table'], $entity);
    }

    return $return;
  }

  /**
   *
   *
   * @see fieldentity_field_storage_load()
   */
  public function loadAsFieldOf($parent_type, $parent_entities, $age, $field_id, $options, $parent_ids) {
    $load_current = $age == FIELD_LOAD_CURRENT;

    $field = (int)$field_id !== 0 ? field_info_field_by_id($field_id) : field_info_field($field_id);
    $field_id = $field['id'];
    $field_name = $field['field_name'];

    $conditions = array(
      'parent_type' => $parent_type,
      ($load_current ? 'parent_id' : 'parent_vid') => $parent_ids,
      'field_id' => $field_id,
      'language' => field_available_languages($parent_type, $field),
    );

    if (empty($options['deleted'])) {
      $conditions['deleted'] = 0;
    }

    $fieldentities = $this->load(NULL, $conditions);

    foreach ($fieldentities as $fieldentity) {
      // Unlike field_sql_storage we don't check cardinality as deltas are
      // controlled very strictly so there is no need to.

      // Add the item to the field values for the entity.
      $parent_entities[$fieldentity->parent_id]->{$field_name}[$fieldentity->language][$fieldentity->delta] = $fieldentity->toArray();
    }
  }

  /**
   *
   *
   * @see fieldentity_field_storage_write()
   */
  public function saveAsFieldsOf($parent_type, $parent_entity, $op, $fields) {
    foreach ($fields as $field_id => $ids) {
      $this->saveAsFieldOf($parent_type, $parent_entity, $op, $field_id);
    }
  }

  /**
   * Saves fieldentities that belong to given parent entity as given field.
   *
   * @param $parent_type
   *   The type of $parent_entity; e.g. 'node' or 'user'.
   * @param $parent_entity
   *   The entity whose field data is being saved.
   * @param $field_id
   *   The ID or name of the field whose data is being saved.
   *
   * @see fieldentity_field_storage_write()
   */
  public function saveAsFieldOf($parent_type, $parent_entity, $op, $field_id) {
    list($parent_id, $parent_vid, $parent_bundle) = entity_extract_ids($parent_type, $parent_entity);

    $field = (int)$field_id !== 0 ? field_info_field_by_id($field_id) : field_info_field($field_id);
    $field_id = $field['id'];
    $field_name = $field['field_name'];

    $all_languages = field_available_languages($parent_type, $field);
    $field_languages = array_intersect($all_languages, array_keys((array) $parent_entity->$field_name));

    $fieldentities = $this->createFromFieldOf($parent_type, $parent_entity, $field_id, $field_languages);

    // Prepare the multi-insert query.
    $do_insert = FALSE;
    $columns = drupal_schema_fields_sql($this->entityInfo['base table']);
    $revision_columns = drupal_schema_fields_sql($this->entityInfo['revision table']);

    $query = db_insert($this->entityInfo['base table'])->fields($columns);
    $revision_query = db_insert($this->entityInfo['revision table'])->fields($revision_columns);

    $record_base = array(
      'parent_type' => $parent_type,
      'parent_id' => $parent_id,
      'parent_vid' => $parent_vid,
      'parent_bundle' => $parent_bundle,
      'field_id' => $field_id,
    );

    $new_fieldentities = array();
    $existing_fieldentities = array();
    $delete_fieldentities = array();
    if ($op == FIELD_STORAGE_UPDATE) {
      $existing_fieldentities = $this->dbLoad(NULL, array('parent_id' => $parent_id, 'field_id' => $field_id, 'deleted' => 0, 'language' => $field_languages));
      $delete_fieldentities = $existing_fieldentities;
    }

    // Calculate deltas, prepare queries.
    $delta_count = array();
    foreach ($fieldentities as $index => $fieldentity) {
      if (!isset($delta_count[$fieldentity->language])) {
        $delta_count[$fieldentity->language] = 0;
      }

      $fieldentity->delta = $delta_count[$fieldentity->language];

      if (empty($fieldentity->{$this->idKey}) || !empty($fieldentity->is_new)) {
        unset($fieldentity->{$this->idKey});
        $fieldentity->is_new = TRUE;

        $new_fieldentities[] = $fieldentity;
      }
      elseif (isset($existing_fieldentities[$fieldentity->{$this->idKey}])) {
        unset($delete_fieldentities[$fieldentity->{$this->idKey}]);
        $do_insert = TRUE;

        $record = $record_base + array(
          $this->idKey => $fieldentity->{$this->idKey},
          'delta' => $fieldentity->delta,
          'language' => $fieldentity->language,
        );
        foreach ($columns as $column) {
          if (isset($fieldentity->$column)) {
            $record[$column] = $fieldentity->$column;
          }
        }
        foreach ($field['columns'] as $column => $attributes) {
          $record[_fieldentity_columnname($field_name, $column)] = isset($fieldentity->$column) ? $fieldentity->$column : NULL;
        }

        $query->values($record);
        if (!empty($parent_vid)) {
          $revision_query->values($record);
        }
      }
      else {
        throw new Exception('Invalid fieldentity parent');
        // TODO
        // The fieldentity has an ID but it doesn't belong to current parent;
        // The deltas of fieldentities belonging to fieldentity's original
        // parent need updating.
        $original_fieldentity = array_pop($this->dbLoad(array($fieldentity->{$this->idKey})));
        $this->dbUpdateDeltas($original_fieldentity->parent_type, $original_fieldentity->parent_id, $original_fieldentity->parent_vid, $original_fieldentity->delta);
      }

      if ($field['cardinality'] != FIELD_CARDINALITY_UNLIMITED && ++$delta_count[$fieldentity->language] == $field['cardinality']) {
        // TODO throwing an exception instead of silently skipping entities might be more correct.
        break;
      }
    }

    $transaction = db_transaction();

    try {
      if ($op == FIELD_STORAGE_UPDATE) {
        // Delete prior fieldentities, that aren't going to be re-saved,
        // properly; Load them first.
        $delete_fieldentities = $this->load(array_keys($delete_fieldentities));

        // Delete and insert, rather than update, in case a value was added.
        // Delete languages present in the incoming $parent_entity->$field_name.
        // Delete all languages if $parent_entity->$field_name is empty.
        $languages = !empty($parent_entity->$field_name) ? $field_languages : $all_languages;
        if ($languages) {
          db_delete($this->entityInfo['base table'])
            ->condition('parent_type', $parent_type)
            ->condition('parent_id', $parent_id)
            ->condition('field_id', $field_id)
            ->condition('language', $languages, 'IN')
            ->execute();
          db_delete($this->entityInfo['revision table'])
            ->condition('parent_type', $parent_type)
            ->condition('parent_id', $parent_id)
            ->condition('parent_vid', $parent_vid)
            ->condition('field_id', $field_id)
            ->condition('language', $languages, 'IN')
            ->execute();
        }

        foreach ($delete_fieldentities as $fieldentity) {
          $this->invoke('delete', $fieldentity);
        }
      }

      foreach ($fieldentities as $fieldentity) {
        $this->invoke('presave', $fieldentity);
      }

      // Save new entities in a seperate query to retrieve serials.
      foreach ($new_fieldentities as $fieldentity) {
        $this->dbSave($fieldentity);

        // add the idKey serial back to entity's field item.
        $parent_entity->{$field_name}[$fieldentity->language][$fieldentity->delta][$this->idKey] = $fieldentity->{$this->idKey};
      }

      // Execute the query if we have values to insert.
      if ($do_insert) {
        $query->execute();
        $revision_query->execute();
      }

      foreach ($fieldentities as $fieldentity) {
        $this->invoke(empty($fieldentity->is_new) ? 'update' : 'insert', $fieldentity);
      }
      // Ignore slave server temporarily.
      db_ignore_slave();

      return TRUE;
    }
    catch (Exception $e) {
      $transaction->rollback($this->entityType, $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
  }

  /**
   * Deletes all fieldentities that belong to given entity as given field.
   *
   * @param $parent_type
   *   The type of $parent_entity; e.g. 'node' or 'user'.
   * @param $parent_entity
   *   The entity whose field data is being deleted.
   * @param $field_id
   *   The ID or name of the field whose data is being deleted.
   *
   * @see fieldentity_field_storage_delete()
   * @see fieldentity_field_storage_purge()
   */
  public function deleteAsFieldOf($parent_type, $parent_entity, $field_id) {
    list($parent_id, $parent_vid, $parent_bundle) = entity_extract_ids($parent_type, $parent_entity);

    $field = (int)$field_id !== 0 ? field_info_field_by_id($field_id) : field_info_field($field_id);
    $field_id = $field['id'];

    $conditions = array(
      'parent_id' => $parent_id,
      'field_id' => $field_id,
    );
    $entities = $this->load(NULL, $conditions);
    $ids = array_keys($entities);

    $transaction = db_transaction();

    try {
      $this->dbDelete($ids);

      foreach ($entities as $entity) {
        $this->invoke('delete', $entity);
      }

      // Ignore slave server temporarily.
      db_ignore_slave();
    }
    catch (Exception $e) {
      $transaction->rollback($this->entityType, $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    $this->resetCache();
  }

  /**
   * Deletes all fieldentity revisions that belong as given field to given entity revision.
   *
   * @param $parent_type
   *   The type of $parent_entity; e.g. 'node' or 'user'.
   * @param $parent_entity
   *   The (revisioned) entity whose field revision data is being deleted.
   * @param $field_id
   *   The ID or name of the field whose revision data is being deleted.
   *
   * @see fieldentity_field_storage_delete_revision()
   */
  public function deleteRevisionAsFieldOf($parent_type, $parent_entity, $field_id) {
    list($parent_id, $parent_vid, $parent_bundle) = entity_extract_ids($parent_type, $parent_entity);

    $field = (int)$field_id !== 0 ? field_info_field_by_id($field_id) : field_info_field($field_id);
    $field_id = $field['id'];

    db_delete($this->entityInfo['revision table'])
      ->condition('parent_type', $parent_type)
      ->condition('parent_id', $parent_id)
      ->condition('parent_vid', $parent_vid)
      ->condition('field_id', $field_id)
      ->execute();
  }

  /**
   *
   * @param unknown_type $parent_type
   * @param unknown_type $parent_entity
   * @param unknown_type $fields
   * @param unknown_type $langcode
   * @return unknown_type
   */
  public function createFromFieldsOf($parent_type, $parent_entity, $fields, $langcode = NULL) {
    $fieldentities = array();
    foreach ($fields as $field_id => $ids) {
      $fieldentities += $this->createFromFieldOf($parent_type, $entity, $op, $field_id);
    }
    return $fieldentities;
  }

  /**
   * Creates fieldentities from the field data of given field of given parent entity.
   *
   * @param $parent_type
   *   The type of $parent_entity; e.g. 'node' or 'user'.
   * @param $parent_entity
   *   The entity whose field data is being used to create fieldentities.
   * @param $field_id
   *   The ID or name of the field whose data is being used to create fieldentities.
   * @param $langcode
   *   (optional) If provided only field data with given language code will be used.
   *
   * @return
   *   Array containing created fieldentities.
   */
  public function createFromFieldOf($parent_type, $parent_entity, $field_id, $langcode = NULL) {
    list($parent_id, $parent_vid, $parent_bundle) = entity_extract_ids($parent_type, $parent_entity);

    $field = (int)$field_id !== 0 ? field_info_field_by_id($field_id) : field_info_field($field_id);
    $field_id = $field['id'];
    $field_name = $field['field_name'];

    if (!empty($langcode)) {
      $field_languages = is_array($langcode) ? $langcode : array($langcode);
    }
    else {
      $all_languages = field_available_languages($parent_type, $field);
      $field_languages = array_intersect($all_languages, array_keys((array) $parent_entity->$field_name));
    }

    $fieldentities = array();
    foreach ($field_languages as $langcode) {
      $items = (array) $parent_entity->{$field_name}[$langcode];
      foreach ($items as $delta => $item) {
        $base = array(
          $this->idKey => isset($item[$this->idKey]) ? $item[$this->idKey] : NULL,
          'parent_type' => $parent_type,
          'parent_id' => $parent_id,
          'parent_vid' => $parent_vid,
          'parent_bundle' => $parent_bundle,
          'field_id' => $field_id,
          'delta' => $delta,
          'language' => $langcode,

          'is_new' => !empty($item[$this->idKey]) ? FALSE : TRUE,
        );

        $fieldentities[] = $this->create($base + $item);
      }
    }

    return $fieldentities;
  }
}

/**
 *
 */
class FieldEntity extends Entity {

  public $parent_type;
  public $parent_id;
  public $parent_vid;
  public $parent_bundle;

  public $field_name;

  public $language;
  public $delta = 0;

  public $deleted = FALSE;
  public $is_new = TRUE;

  public function __construct(array $values = array(), $entityType = NULL) {
    if (!isset($this->parent_type) && !isset($values['parent_type'])) {

      $this->parent_type = 'node';
      //throw new Exception('Cannot create an instance of FieldEntity without a specified parent entity.');
    }

    if (!isset($values['parent_type'], $values['parent_id'])) {
      //throw new Exception('Cannot create an instance of FieldEntity without a specified parent entity.');
    }

    if (!isset($values['parent_bundle'])) {
      // TODO retrieve automaticly
    }

    if (!isset($this->field_name) && !isset($values['field_name'])) {
      if (isset($values['field_id'])) {
        $field = field_info_field_by_id($values['field_id']);
        $this->field_name = $field['field_name'];
      }
      elseif (isset($this->field_id)) {
        $field = field_info_field_by_id($this->field_id);
        $this->field_name = $field['field_name'];
      }
      else {
        throw new Exception('Cannot create an instance of FieldEntity without a specified field name.');
      }
    }

    parent::__construct($values, $entityType);
  }

  public function toArray() {
    return (array) $this;
  }


  /**
   * Returns the label.
   */
  public function label() {
    // @todo make configurable.
    if ($instance = $this->instanceInfo()) {
      $field = $this->fieldInfo();
      if ($field['cardinality'] == 1) {
        return $instance['label'];
      }
      elseif ($this->item_id) {
        return t('!instance_label @count', array('!instance_label' => $instance['label'], '@count' => $this->delta() + 1));
      }
      else {
        return t('New !instance_label', array('!instance_label' => $instance['label']));
      }
    }
    return t('Unconnected field-collection item');
  }

  /**
   * Returns the path used to view the entity.
   */
  public function path() {
    if ($this->item_id) {
      return field_collection_field_get_path($this->fieldInfo) . '/' . $this->item_id;
    }
  }

  /**
   * Returns the URI as returned by entity_uri().
   */
  public function uri() {
    $entity = entity_load($combo->parent_type, $combo->parent_id);

    return entity_uri($combo->parent_type, $entity);
  }
}
