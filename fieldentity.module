<?php

/**
 * @file
 * FieldEntity implementation of the field storage API.
 */

require_once dirname(__FILE__) . '/fieldentity.field.inc';

/**
 * Implements hook_help().
 */
function fieldentity_help($path, $arg) {
  switch ($path) {
    case 'admin/help#fieldentity':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The FieldEntity module provides an API to use Field module fields as fieldable entities. It contains an dedicated entity controller and an field storage backend that delegates field storage to its entity controller.') . '</p>';
      return $output;
  }
}

/**
 * Determines whether any fieldentity with the same type has any data.
 *
 * @param $field_type
 *   A (entity)field type name.
 *
 * @return
 *   TRUE if a field with the same type has data for any entity; FALSE otherwise.
 */
function fieldentity_has_data($field_type) {
  $table = _fieldentity_tablename($field_type);
  return (bool) db_query_range('SELECT COUNT(*) FROM {' . $table . '}', 0, 1)->fetchField();
}

/**
 * Determines if all of a fieldentity's (sub)fields are empty.
 */
function fieldentity_is_empty($item, $field) {
  $fieldentity_type = $field['fieldentity'];
  $fieldentity = entity_create($fieldentity_type, $item);
  $subfield_instances = field_info_instances($fieldentity_type, $field['field_name']);

  foreach ($subfield_instances as $instance) {
    $subfield_name = $instance['field_name'];
    $subfield = field_info_field($subfield_name);

    // Determine the list of languages to iterate on.
    $available_languages = field_available_languages($fieldentity_type, $subfield);
    $languages = _field_language_suggestion($available_languages, NULL, $subfield_name);

    foreach ($languages as $langcode) {
      $items = isset($fieldentity->{$subfield_name}[$langcode]) ? $fieldentity->{$subfield_name}[$langcode] : array();
      $function = $subfield['module'] . '_field_is_empty';
      foreach ($items as $subfield_item) {
        if (!$function($subfield_item, $subfield)) {
          // At least one (sub)field is not empty; the fieldentity is not empty.
          return FALSE;
        }
      }
    }
  }

  return TRUE;
}

/**
 * Perform field validation against the field data in an fieldentity.
 */
function fieldentity_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  $fieldentity_type = $field['fieldentity'];

  foreach ($items as $delta => $item) {
    $fieldentity = entity_create($fieldentity_type, $item);

    // Perform (sub)field-level validation.
    try {
      field_attach_validate($fieldentity_type, $fieldentity);
    }
    catch (FieldValidationException $e) {
      // Pass (sub)field-level validation errors back to widget for accurate
      // error flagging.
      $errors[$field['field_name']][$langcode][$delta][] = array(
        'error' => 'fieldentity_fields',
        'message' => t('%name: invalid.', array('%name' => $instance['label'])),
        'errors' => $e
      );
    }
  }
}
